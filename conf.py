# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Observing Plans'
copyright = '2022 the KAGRA Collaboration, the LIGO Scientific Collaboration, and the Virgo Collaboration'
author = 'the KAGRA Collaboration, the LIGO Scientific Collaboration, and the Virgo Collaboration'
html_title = 'IGWN | Observing Plans'

# The full version, including alpha/beta/rc tags
release = '2022.06.19'

html_last_updated_fmt = "%H:%M %B %d %Y"

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.extlinks',
    'sphinx_immaterial_igwn',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for extlinks extension ------------------------------------------

extlinks = {
    'arxiv': ('https://arxiv.org/abs/%s', 'arXiv:%s'),
    'dcc': ('https://dcc.ligo.org/%s/public', 'LIGO-'),
    'doi': ('https://doi.org/%s', 'doi:%s')
}

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_immaterial_igwn'
html_path = ["."]
html_theme_options = {
    "repo_name": "plan",
    "repo_url": "https://git.ligo.org/observing/plan",
    "repo_type": "gitlab",
    "site_url": "https://observing.docs.ligo.org/plan/",
}
html_last_updated_fmt = ""

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
