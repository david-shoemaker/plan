Glossary
========

.. glossary::

    BBH
        Binary black hole, a binary system composed of two black holes. See
        :term:`BH`.

    BH
        Black hole.

    BNS
        Binary neutron star, a binary system composed of two neutron stars.
        See :term:`NS`.

    burst
        In the context of gravitational waves, a signal candidate that is
        detected without a template and without prior knowledge of the
        waveform. Examples of potential sources of gravitational-wave bursts
        include high mass :term:`BBH` mergers, core-collapse supernovae, and
        cosmic string cusps.

    KAGRA
        Kamioka Gravitational Wave Detector (see `KAGRA home page
        <https://gwcenter.icrr.u-tokyo.ac.jp/en/>`_), an underground
        gravitational-wave detector in the Kamioka mine in Japan.

    LHO
        LIGO Hanford Observatory (see `LHO observatory home page
        <https://www.ligo.caltech.edu/WA>`_), site of a 4 km gravitational-wave
        detector in Hanford, Washington, USA.

    LLO
        LIGO Livingston Observatory (see `LLO observatory home page
        <https://www.ligo.caltech.edu/LA>`_), site of a 4 km gravitational-wave
        detector in Livingston, Louisiana, USA.

    NS
        Neutron star.

    NSBH
        Neutron star black hole, a binary system composed of one neutron star
        and one black hole. See :term:`NS`, :term:`BH`.

    range
        A figure of merit to describe the sensitivity of a gravitational-wave
        detector to a given source population at cosmologically significant
        distances. It is defined as the radius :math:`R` of a Euclidean sphere
        with the volume equal to the :term:`sensitive volume` :math:`V_z`. It
        may be written as:

        .. math::

           R = \left(\frac{3 V_z}{4 \pi}\right)^{1/3}.

    sensitive volume
        A figure of merit for the sensitivity of a gravitational-wave detector
        or a network of detectors. It is defined as the space-time volume
        surveyed per unit detector time, and may be expressed as (cf.
        [#DistanceMeasuresInGWCosmology]_):

        .. math::

           V_\mathrm{z}
               = \frac{
                   \int_{z < z^*(\Theta)} p(\Theta) \frac{dV_C}{dz} \frac{dz}{1 + z}
               }{\int p(\Theta) d\Theta}.

        Here, :math:`\Theta` is the set of parameters that describe the
        gravitational-wave signal (merger time, sky location, orbital elements,
        masses, and spins) and :math:`p(\Theta)` is the redshift-independent
        population model for those parameters. The term :math:`\frac{dV_C}{dz}`
        is differential comoving volume per unit redshift. The function
        :math:`z^*(\Theta)` is the *threshold redshift*, or the redshift at
        which a binary with parameters :math:`\Theta` is just at the limit of
        detection. The factor of :math:`{1 + z}` in the denominator accounts
        for time dilation from the source frame to the detector frame.

        If a population of sources occurs at a fixed rate per unit comoving
        volume per unit proper time :math:`\dot{n}`, then the rate of observed
        events in the detector frame is :math:`\dot{n} V_z`.

    Virgo
        Virgo Observatory (see `Virgo observatory home page
        <http://www.virgo-gw.eu>`_), site of a 3 km gravitational-wave detector
        in Cascina, Italy.

.. include:: /journals.rst

.. [#DistanceMeasuresInGWCosmology]
   Chen, H.-Y., Holz, D. E., et al. 2017, *Distance measures in
   gravitational-wave astrophysics and cosmology*. :arxiv:`1709.08079`
