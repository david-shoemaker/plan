LIGO, VIRGO AND KAGRA OBSERVING RUN PLANS
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. only:: html

    .. toctree::
       :hidden:

       glossary

(15 May 2023 update; next update 15 June 2023 or sooner)

We currently plan to start O4 on 24 May 2023. The observing run will last 20 calendar months including up to 2 months of commissioning breaks for maintenance.

*For LIGO Hanford and Livingston*, the detectors are undergoing final preparation in the ER15 engineering run. Work continues on increasing the duty cycle and stability, and on detector calibration and automation. LIGO plans to start the observing run on 24 May 2023. Current sensitivities yield detection ranges of 130-150 Mpc for binary neutron star coalescence, and we do not expect these will increase significantly before the start of O4. This is below the target range of 160-190 Mpc. We will continue to seek ways to improve the sensitivity during the run.

*For Virgo*,  Virgo will not enter O4 on May 24, but will continue commissioning to address a damaged mirror that is limiting the performance, and to improve sensitivity. We anticipate that toward the end of June, after the work on the west-input and north-end mirrors, we will be able to provide more precise indications for the path forward. 

*For KAGRA*, 1 Mpc sensitivity was obtained. KAGRA continues commissioning to improve sensitivity. KAGRA plans to start the observing run as foreseen on 24 May after performing the engineering run for one week just before the start of O4. The influence of earthquakes swarm in the vicinity of the KAGRA site is a concern.

As noted in our previous update, the longer O4 run facilitates upgrade plans for the O5 observing run – the sensitivity improvement for O5 comes primarily from lower noise mirror coatings that are still in the development phase, and are not expected to be available by the original O4 end date. The additional observing time will increase the scientific output of O4, while the coating development is being finalized and the test masses required for O5 are being coated. 

Please check the OpenLVKEM pages for the timing and coordinates for telemeetings. The schedule of calls is posted at the OpenLVKEM wiki (see below), along with Agenda and pointers to materials.



Reference Material:

- OpenLVKEM Wiki pages https://wiki.gw-astronomy.org/OpenLVEM


Timeline
--------

The gravitational-wave observing schedule is divided into Observing Runs, down time for construction and commissioning, and transitional Engineering Runs between commissioning and observing runs. The current best understanding of the long-term observing schedule is shown
below. Since BNS (Binary Neutron Star) mergers are a well-studied class of gravitational-wave signals, this figure gives the BNS range for for a single-detector SNR threshold of 8 in each observing run.

.. figure:: _static/ObsScen_timeline.png
   :alt: Long-term observing schedule




The O5 start dates, duration, and sensitivities are current best guesses, and will likely be adjusted as we approach that run. 


Live Status
-----------

There are a handful of public web pages that report live status of the
LIGO/Virgo detectors and alert infrastructure.

*  | **Detector Status Portal**: Daily summary of detector performance.
   | https://www.gw-openscience.org/detector_status/

*  | **GWIStat**: Real-time detector up/down status.
   | https://ldas-jobs.ligo.caltech.edu/~gwistat/gwistat/gwistat.html

*  | **LIGO Data Grid Status**: Live dashboard showing up/down status of the
     detectors and online analyses. Status of the LIGO/Virgo alert pipeline is
     indicated by the "EMFollow" box.
   | https://monitor.ligo.org/gwstatus


